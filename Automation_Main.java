import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class Automation_Main {

	static Properties prop;
	static PrintStream log_path;

	public static void main(String[] args) throws Exception {

		prop = new Properties();
		prop.load(new FileInputStream(new File(args[0])));

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date currDate = new Date();
		String date = currDate.toString().replaceAll(":", "-");
		date = date.replaceAll(":", "");

		log_path = new PrintStream(new FileOutputStream(prop.getProperty("LOG_PATH") + date + ".log"));

		log_path.println("=================================================================================");
		log_path.println("---------------------------------------------------------------------------------");
		log_path.println("Utility started at " + currDate);

		int retryCount = Integer.parseInt(prop.getProperty("RETRY_COUNT"));

		if (prop.getProperty("SERVICES") == null || "".equalsIgnoreCase(prop.getProperty("SERVICES"))) {
			log_path.println("There is no services-SERVICES listed in the config file. Exiting..");
			log_path.flush();
			log_path.close();
			System.exit(0);
		}

		String[] servicesArray = prop.getProperty("SERVICES").split(",");
		for (int i = 0; i < servicesArray.length; i++) {
			String[] serviceList = servicesArray[i].split(":");
			if (!isPortActive(serviceList[0], Integer.parseInt(serviceList[1]))) {

				try {
					stopService(serviceList[2]);
				} catch (Exception e) {
					log_path.println(e.getMessage());
					log_path.flush();
					log_path.close();
					System.exit(0);
				}

			}
		}

	}

	public static boolean isPortActive(String serverName, int port) {
		Socket pingSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;

		try {
			pingSocket = new Socket(serverName, port);
			out = new PrintWriter(pingSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
			System.out.println("port is open");
			out.println("ping");
			System.out.println(in.readLine());
			out.close();
			in.close();
			pingSocket.close();
			return true;
		} catch (Exception e) {
			System.out.println("port is either wrong or not open");
			return false;
		}
	}

	public static void stopService(String serviceName) throws Exception {

		log_path.println("Trying to stop the service " + serviceName);
		String[] scriptStop = { "net", "stop", serviceName };
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(scriptStop);
		log_path.println("Attempting to stop the Service " + serviceName);
		process.waitFor();
		Thread.sleep(10000L);
	}
}
